**1. Autor:** Francisco Javier Molina Fern�ndez.

**2. Tema:** Real Betis Balompi�.

**3. Instrucciones para generar la Tarea 3:**
    1. Con el programa Notepad++ he creado un html llamado index.html, y lo he divido en 3 filas y la fila del centro en 2 columnas.
    2. En la fila de arriba y la de abajo, le he puesto una imagen creada por mi en el paint. La fila del centro est� dividida dos columnas y cada una de ellas est� enlazada a un documento html.
    3. La columna derecha tiene un fondo hecho con css y un enlace de un video de youtube. Y la parte izquierda tiene un fondo verde tambi�n puesto con css y un men� creado a trav�s de una lista no numeral, donde al hacer clik con el rat�n en cada nombre, le lleva a un html que tiene asignado. Vi�ndose esta nueva p�gina en la columna de la derecha.

**4. Ver la Tarea 3:** Simplemente abrir el archivo "*INDEX.html*" con el navegador *Google Chrome*.

**CONCLUSIONES:** No ten�a ning�n conocimiento en html, aunque es f�cil hacer una p�gina web si tienes un buen manual a cerca. Es muy importante hacer muchas para ir cogiendo practica, ya que son muchas etiquetas y muchas f�rmmulas que con que se te olvide poner algo o lo pongas mal no sale el resultado que uno quiere. Pero con un poco de paciencia aunque se tengan pocos conocimientos, se pueden hacer buenos trabajos.